//function to Sort user details based on last names into alphabetical order

function problem3(users) {

    if (Array.isArray(users) !== true) {
        return [];
    }


    users.sort((a, b) => {
        const nameA = a.last_name.toUpperCase();
        const nameB = b.last_name.toUpperCase();

        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        return 0;
    });

    return users.sort();

}

module.exports = problem3;
