// find user details

function problem1(users, id) {

    // if id is not a number return empty array
    if (typeof (id) !== "number") {
        return []
    }

    for (let index = 0; index < users.length; index++) {
        if (users[index].id === id) {
            console.log(`${users[index].first_name} ${users[index].last_name} is a ${users[index].gender} and can be contacted on ${users[index].email}`);
            return users[index];   // if found return data
        }
    }

    return [];

}

module.exports = problem1;