// this function return email id from user's data

function problem4(users){

    if(Array.isArray(users)!==true){
        return [];
    }

    let emailList = []

    for(let index=0; index<users.length; index++){
        emailList.push(users[index].email);         // push email id
    }

    return emailList;
}

module.exports = problem4;