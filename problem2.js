// find last user details

function problem2(users){
    if(Array.isArray(users)!==true){
        return [];
    }

    let lastIndex = users.length-1;
    let lastUser ="";

    lastUser = `Last user is ${users[lastIndex].first_name} ${users[lastIndex].last_name} and can be contacted on ${users[lastIndex].email}`;
    return lastUser;
}

module.exports = problem2;