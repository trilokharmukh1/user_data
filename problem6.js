// this function find out how many users are male and return the array of names and emails  

function problem6(users) {

    if (Array.isArray(users) !== true) {
        return [];
    }
    
    let usersGender = [];  // to store all gender arrays 
    let maleGender = [];
    let femaleGender = [];
    let polyGnder = [];
    let bigender = [];
    let genderQueer = [];
    let genderFluid = [];
    let aGender = [];

    for (let index = 0; index < users.length; index++) {

        if (users[index].gender === "Male") {
            maleGender.push(users[index]);

        } else if (users[index].gender === "Female") {
            femaleGender.push(users[index]);

        } else if (users[index].gender === "Polygender") {
            polyGnder.push(users[index]);

        } else if (users[index].gender === "Bigender") {
            bigender.push(users[index]);

        } else if (users[index].gender === "Genderqueer") {
            genderQueer.push(users[index]);

        } else if (users[index].gender === "Genderfluid") {
            genderFluid.push(users[index]);

        } else if (users[index].gender === "Agender") {
            aGender.push(users[index]);

        }
    }

    //push all genders array in userGender 
    usersGender.push(maleGender, femaleGender, polyGnder, bigender, genderQueer, genderFluid, aGender);

    return JSON.stringify(usersGender);
}

module.exports = problem6;