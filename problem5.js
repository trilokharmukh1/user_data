// this function find out how many users are male and return the array of names and emails  

function problem5(users) {

    if (Array.isArray(users) !== true) {
        return [];
    }

    let nameAndEmail = [];  // to store users details 

    for (let index = 0; index < users.length; index++) {

        if (users[index].gender === "Male") {
            nameAndEmail.push({
                first_name: `${users[index].first_name}`,
                last_name: `${users[index].last_name}`,
                email: `${users[index].email}`
            })
        }
    }

    return nameAndEmail;
}

module.exports = problem5;